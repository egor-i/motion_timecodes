import time
import cv2
from skimage.metrics import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np

vidcap = cv2.VideoCapture('test_video.mp4')

success,old_image = vidcap.read()
old_image = cv2.resize(old_image, (640, 480))
old_image_gray = cv2.cvtColor(old_image, cv2.COLOR_BGR2GRAY)
similarity_array = []

count = 0;
start = time.time()

while True:
    vidcap.set(cv2.CAP_PROP_POS_MSEC, count*1000)

    success,new_image = vidcap.read()
    new_image = cv2.resize(new_image, (640, 480))
    if not success:
        break
    new_image_gray = cv2.cvtColor(new_image, cv2.COLOR_BGR2GRAY)
    similarity_array.append(ssim(old_image_gray, new_image_gray))
    old_image_gray = new_image_gray
    if not(count%20):
        print("{} seconds of video processed".format(count))
    count += 1

end = time.time()
print("Time taken to process the whole video: ",end - start)




fig = plt.figure(figsize=(12, 6))
plt.plot(similarity_array)
fig.savefig('video_changes.jpg')

with open('video_changes.txt', 'w') as f:
    for item in similarity_array:
        f.write("%s\n" % item)
