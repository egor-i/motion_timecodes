import time
import cv2
from skimage.metrics import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np

vidcap = cv2.VideoCapture('test_video.mp4')
thresh = 0.95

vidcap.set(cv2.CAP_PROP_POS_MSEC, 0)
sequence_length = 1
sequence_start = 0

with open('video_changes.txt') as f:
    lines = f.read().splitlines()
similarity_array = list(map(float, lines))

for i, el in enumerate(similarity_array):
    framerate_time = i+1
    if el < thresh:
        if sequence_length:
            sequence_length = sequence_length + 1
        else:
            sequence_length = 1
            sequence_start = framerate_time
    else:
        if sequence_length:
            vidcap.set(cv2.CAP_PROP_POS_MSEC, sequence_start*1000)
            success,image = vidcap.read()
            if sequence_start == framerate_time-1:
                cv2.imwrite(time.strftime('%H-%M-%S', time.gmtime(sequence_start)) + ".jpg", image)
                print(time.strftime('%H-%M-%S', time.gmtime(sequence_start)))
            else:
                cv2.imwrite(time.strftime('%H-%M-%S - ', time.gmtime(sequence_start))+ time.strftime('%H-%M-%S', time.gmtime(framerate_time-1)) + ".jpg", image)
                print(time.strftime('%H-%M-%S - ', time.gmtime(sequence_start))+ time.strftime('%H-%M-%S', time.gmtime(framerate_time-1)))
            sequence_length = 0